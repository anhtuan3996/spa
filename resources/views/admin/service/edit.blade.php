@extends('admin.layouts.app')
@section('title', 'Edit product')
@section('content')
    <div class="inner-block">
        <div class="product-block">
            <div class="pro-head">
                <h2>{{trans('messages.edit_product')}}</h2>
            </div>
            <div class="error">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 product-grid">
                <form action="{{route('service.edit', $service->id)}}" method="post" enctype="multipart/form-data">
                    {{ method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Tên dịch vụ</label>
                        <input type="text" class="form-control <?php echo $errors->has('name') ? 'input-error' : '';?>" name="name" value="{{$service->name}}" placeholder="Nhập tên sản phẩm">
                    </div>
                    <div class="form-group">
                        <label for="">Giá dịch vụ</label>
                        <input type="text" class="form-control <?php echo $errors->has('price') ? 'input-error' : '';?>" name="price" value="{{$service->price}}" placeholder="Nhập giá sản phẩm">
                    </div>
                    <div class="form-group">
                        <label for="">Phụ cấp</label>
                        <input type="number" class="form-control <?php echo $errors->has('bonus') ? 'input-error' : '';?>" name="bonus" value="{{$service->price}}" placeholder="Nhập lương thưởng cho nhân viên">
                    </div>
                    <div class="form-group">
                        <label for="">Thời gian</label>
                        <input type="number" class="form-control <?php echo $errors->has('time') ? 'input-error' : '';?>" name="time" value="{{$service->time}}" placeholder="Thời gian hoàn thành">
                    </div>
                    <div class="form-group">
                        <label for="">Đơn vị thời gian</label>
                        <select name="unit_time">
                            <option value="{{\App\Service::HOURS}}">Giờ</option>
                            <option value="{{\App\Service::MINUTE}}" {{$service->unit_time == \App\Service::MINUTE ? 'selected': ''}}>Phút</option>
                            <option value="{{\App\Service::SECONDS}}" {{$service->unit_time == \App\Service::SECONDS ? 'selected': ''}}>Giây</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="show_image">
                            <img src="{{asset('/storage/service/'.$service->image)}}" class="img-responsive" alt="{{$service->name}}" style="max-width: 300px;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">{{trans('messages.image')}} </label>
                        <input type="file" class="form-control <?php echo $errors->has('image-service') ? 'input-error' : '';?>" name="image-service">
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea class="trumbowyg" name="description">{{$service->description}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-warning submit-form">Chỉnh sửa dịch vụ</button>
                </form>
                <form action="{{route('service.delete', $service->id)}}" class="form-edit" method="post">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger" value="Xóa dịch vụ">
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection