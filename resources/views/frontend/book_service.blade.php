@extends('frontend.layouts.app')
@section('title', 'index')
@section('content')
    <div class="container">
        <div class="error">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <table class="table table-bordered" style="margin-top: 50px;">
            <thead>
            <tr>
                <th>Tên dịch vụ</th>
                <th>Giá tiền</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$service->name}}</td>
                <td>{{number_format($service->price, 0 ,',', '.')}} vnđ</td>
            </tr>

            </tbody>
        </table>
            <form action="{{route('book.service.add', $service->id)}}" method="post" style="margin: 30px 0px;">
                {{ csrf_field() }}
                @auth
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('messages.email_lable')}}</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" value=" {{\Auth::user()->email}}" name="email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('messages.first_name_last_name_lable')}}</label>
                        <input type="text" class="form-control" id="exampleInputPassword1"  name="name" value=" {{\Auth::user()->name}}">
                    </div>
                @else
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{trans('messages.email_lable')}}</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" value="{{old('email')}}" placeholder="{{trans('messages.email_lable')}}" name="email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{trans('messages.first_name_last_name_lable')}}</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.first_name_last_name_lable')}}" name="name" value="{{old('name')}}">
                    </div>
                @endauth
                <div class="form-group">
                    <label for="exampleInputPassword1">Thời gian</label>
                    <input type="text" class="form-control" id="datepicker" placeholder="Thời gian" name="time" value="{{old('address')}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{trans('messages.phone')}}</label>
                    <input type="number" class="form-control" id="exampleInputPassword1" placeholder="{{trans('messages.phone')}}" name="phone" value="{{old('phone')}}">
                </div>
                <button type="submit" class="btn btn-success">{{trans('messages.customer_order')}}</button>
            </form>

    </div>
@endsection