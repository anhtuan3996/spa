<?php

namespace App\Http\Controllers;

use App\Product;
use App\Brand;
use App\Service;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $services = Service::select('id','name', 'price', 'image')->limit(4)->orderBy('id', 'DESC')->get();
        return view('frontend.index', ['services'=>$services]);
    }

    public function detail($id)
    {
        $product = Product::select('id', 'quantity', 'image', 'name', 'desc', 'price');
        $product = $product->where('id', $id)->firstOrFail();
        return view('frontend.product_detail',['product' =>$product]);
    }

    public function about()
    {
        return view('frontend.about');
    }
    public function contact()
    {
        return view('frontend.contact');
    }
}
