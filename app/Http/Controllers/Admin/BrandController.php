<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddBrandRequest;
use App\Brand;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::select('id', 'name', 'updated_at')
            ->orderBy('id', 'DESC')
            ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.brand.index', ['brands' =>$brands]);
    }

    public function form_add()
    {
        return view('admin.brand.add');
    }

    public function add(AddBrandRequest $request)
    {
        Brand::insert(
            [
                'name' => $request->input('name'),
                'description' => $request->input('desc'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        return redirect(route('brand.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'Nhà cung cấp']));
    }
    public function form_edit($id)
    {

       $brand = Brand::select('id', 'name', 'description')
            ->where('id', trim($id))->firstOrFail();

        return view('admin.brand.edit', ['brand' => $brand]);
    }
    public function edit($id, AddBrandRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('desc'),
            'updated_at' => Carbon::now()
        ];
        Brand::where('id', trim($id))->update($data);

        return redirect(route('brand.index'))
            ->with('alert-success', trans('messages.successfully_updated', ['name' => 'Nhà cung cấp']));
    }

    public function delete($id)
    {
        Brand::findOrFail(trim($id));
        Brand::where('id', trim($id))->delete();

        return redirect(route('brand.index'))
            ->with('alert-success', trans('messages.successfully_deleted', ['name' => 'Nhà cung cấp']));
    }

    public function image($id)
    {
        $photo = Brand::where('id', trim($id))
            ->first();

        if (empty($photo)) {
            return abort(404);
        }
        $path = storage_path().'/app/public/brand/'.$photo->image;
        echo file_get_contents($path);
        return true;

    }
}
