<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\MemberRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MemberRegisterController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public function getRegister()
    {
        return view('frontend.auth.register');
    }

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function postRegister(MemberRequest $request)
    {
        Customer::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
        return redirect(route('frontend.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => trans('thành viên')]));
    }

}
